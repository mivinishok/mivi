package com.codonsoft.myapplication;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.codonsoft.myapplication.json.Appointment;
import com.codonsoft.myapplication.json.ComplaintsCust;
import com.codonsoft.myapplication.json.jsonmain.ComplaintsCustMain;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    static final String KEY_Name = "ON"; // parent node
    static final String KEY_Password = "andriodtest";
    static String KEY_Date = "08/02/2018";
    static final String KEY_COST = "cost";
    static final String KEY_DESC = "description";
    private SQLiteDatabase db;

    static String name="";
    static int length=0;
    static String[][] data = new String[15][13];;
    List<ComplaintsCust> complaintsCustList = new ArrayList<>();
    List<Appointment> appointmentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Arrays.fill(data, null);
        data = new String[15][13];
        db=openOrCreateDatabase("imageDB", Context.MODE_PRIVATE, null);

        db.execSQL("CREATE TABLE IF NOT EXISTS name(name VARCHAR);");

        String query1 = "DELETE FROM name;";
        db.execSQL(query1);


        db.execSQL("CREATE TABLE IF NOT EXISTS complaindetails(CustomerTitle VARCHAR," +
                " CustomerForename VARCHAR," +
                " CustomerSurname VARCHAR, " +
                "CustomerCompanyName VARCHAR," +
                " BuildingName VARCHAR, " +
                "CustomerStreet VARCHAR, " +
                "CustomerAddressArea VARCHAR, " +
                "CustomerAddressTown VARCHAR, " +
                "CustomerCounty VARCHAR, " +
                "ChargeType VARCHAR, " +
                "JobType VARCHAR, " +
                "CustomerPostcode VARCHAR, " +
                "CustomerMobileNo VARCHAR);");





        query1 = "DELETE FROM complaindetails;";
        db.execSQL(query1);


        final Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {



                new Thread(new Runnable() {
                    public void run() {
                        try {
                            String urlParameters  = "<GetAppointmentDetails>\n"+
                                    "<SLUsername>ON</SLUsername>;\n"+
                                    "<SLPassword>andriodtest</SLPassword>;\n"+
                                    "<CurrentDate>"+KEY_Date+"</CurrentDate>;\n"+
                                    "</GetAppointmentDetails>;";
                            byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
                            int    postDataLength = postData.length;
                            String request        = "https://www.skylinecms.co.uk/alpha/RemoteEngineerAPI/GetAppointmentDetails/";
                            URL url            = new URL( request );
                            HttpURLConnection conn= (HttpURLConnection) url.openConnection();
                            conn.setDoOutput( true );
                            conn.setDoInput(true);
                            conn.setInstanceFollowRedirects( false );
                            conn.setRequestMethod( "POST" );
                            conn.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
                            conn.setRequestProperty( "charset", "utf-8");
                            conn.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
                            conn.setUseCaches( false );
                            try( DataOutputStream wr = new DataOutputStream( conn.getOutputStream())) {
                                wr.write( postData );
                            }
                            ;

                            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                            System.out.println("Inside checkCredentials119");


                            String returnString = in.readLine();



                            JSONObject jsonObj = null;
                            try {
                                jsonObj = XML.toJSONObject(returnString);
                            } catch (JSONException e) {
                                Log.e("JSON exception", e.getMessage());
                                e.printStackTrace();
                            }

                            JSONObject arr = jsonObj.getJSONObject("Response");
                            for (int i = 0; i < 1; i++)
                            {
                                String post_id = arr.getString("ResponseCode");

                                if(post_id.contains("SC0001"))
                                {
                                    ObjectMapper mapper = new ObjectMapper();
                                    try
                                    {
                                        String s  = jsonObj.toString();
                                        ComplaintsCust emp = mapper.readValue(jsonObj.toString(), ComplaintsCust.class);
                                        System.out.println(emp);

                                        Map<String, Object> temp = new HashMap<String, Object>();

                                        emp.getResponse().getAppointments();


                                        ;

                        /*for(Map.Entry<String, Object> entry: temp.entrySet()) {
                            System.out.println(entry.getKey() + " : " + entry.getValue());
                        }*/
                                        //HashMap<ComplaintsCustMain>

                               /* db=openOrCreateDatabase("imageDB", Context.MODE_PRIVATE, null);
                                ContentValues cv1 = new ContentValues();
                                cv1.put("id", emp.getResponse().getDefaults().getFullName());
                                db.insert("name", null, cv1);*/
                                        name=emp.getResponse().getDefaults().getFullName();
                                        length=emp.getResponse().getAppointments().getAppointment().size();
                                        for(int ii=0;ii<emp.getResponse().getAppointments().getAppointment().size();ii++) {
                                            // ContentValues cv = new ContentValues();

                                            data[ii][0]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerTitle().toString();
                                            data[ii][1]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerForename().toString();
                                            data[ii][2]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerSurname().toString();
                                            data[ii][3]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCollectionCompanyName().toString();
                                            data[ii][4]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerBuildingName().toString();
                                            data[ii][5]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCollectionStreet().toString();
                                            data[ii][6]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerAddressArea().toString();
                                            data[ii][7]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerAddressTown().toString();
                                            data[ii][8]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerCounty().toString();
                                            data[ii][9]= emp.getResponse().getAppointments().getAppointment().get(ii).getWarrantyDetails().getChargeType().toString();
                                            data[ii][10]= emp.getResponse().getAppointments().getAppointment().get(ii).getWarrantyDetails().getChargeType().toString();
                                            data[ii][11]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerPostcode();
                                            data[ii][12]= emp.getResponse().getAppointments().getAppointment().get(ii).getCustomerDetails().getCustomerMobileNo();


                                        }
                                    }


                                    catch (JsonGenerationException e)
                                    {
                                        e.printStackTrace();
                                    } catch (JsonMappingException e) {
                                        e.printStackTrace();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                }

                                else
                                {
                                    length=0;
                                }

                            }




                            System.out.println("Inside checkCredentials119");
/*System.out.println(returnString);

                    Pattern pattern = Pattern.compile("<%=(.*?)%>");
                    String[] result = pattern.split(returnString);
                    System.out.println(Arrays.toString(result));*/

                        } catch (final Exception e) {
                            //progressDialog.dismiss();
                            System.out.print("Exception");
                            e.printStackTrace();
                        }
                    }
                }).start();

            }
        });


        final Button button = findViewById(R.id.button3);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                if(length>0)
                {
                    Intent myIntent = new Intent(MainActivity.this, Main2Activity.class);
                    MainActivity.this.startActivity(myIntent);
                }
                // Code here executes on main thread after user presses button
            }
        });

        final Button button1 = findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                    Intent myIntent = new Intent(MainActivity.this, Main3Activity.class);
                    MainActivity.this.startActivity(myIntent);

                // Code here executes on main thread after user presses button
            }
        });
    }
}
