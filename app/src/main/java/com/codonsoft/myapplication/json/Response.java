
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ResponseCode",
    "APIName",
    "ResponseDescription",
    "Defaults",
    "Appointments"
})
public class Response {

    @JsonProperty("ResponseCode")
    private String responseCode;
    @JsonProperty("APIName")
    private String aPIName;
    @JsonProperty("ResponseDescription")
    private String responseDescription;
    @JsonProperty("Defaults")
    private Defaults defaults;
    @JsonProperty("Appointments")
    private Appointments appointments;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ResponseCode")
    public String getResponseCode() {
        return responseCode;
    }

    @JsonProperty("ResponseCode")
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    @JsonProperty("APIName")
    public String getAPIName() {
        return aPIName;
    }

    @JsonProperty("APIName")
    public void setAPIName(String aPIName) {
        this.aPIName = aPIName;
    }

    @JsonProperty("ResponseDescription")
    public String getResponseDescription() {
        return responseDescription;
    }

    @JsonProperty("ResponseDescription")
    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    @JsonProperty("Defaults")
    public Defaults getDefaults() {
        return defaults;
    }

    @JsonProperty("Defaults")
    public void setDefaults(Defaults defaults) {
        this.defaults = defaults;
    }

    @JsonProperty("Appointments")
    public Appointments getAppointments() {
        return appointments;
    }

    @JsonProperty("Appointments")
    public void setAppointments(Appointments appointments) {
        this.appointments = appointments;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
