
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Appointment {

    @SerializedName("AdditionalEngineerID")
    @Expose
    private String additionalEngineerID;
    @SerializedName("SLAppID")
    @Expose
    private String sLAppID;
    @SerializedName("SortOrder")
    @Expose
    private String sortOrder;
    @SerializedName("AppointmentType")
    @Expose
    private String appointmentType;
    @SerializedName("ServiceProviderID")
    @Expose
    private String serviceProviderID;
    @SerializedName("SendCustomerReceiptEmail")
    @Expose
    private String sendCustomerReceiptEmail;
    @SerializedName("TimeSlot")
    @Expose
    private String timeSlot;
    @SerializedName("AppointmentDefaults")
    @Expose
    private AppointmentDefaults appointmentDefaults;
    @SerializedName("JobDetails")
    @Expose
    private JobDetails jobDetails;
    @SerializedName("JobFaultCodes")
    @Expose
    private List<Object> jobFaultCodes = null;
    @SerializedName("CustomerDetails")
    @Expose
    private CustomerDetails customerDetails;
    @SerializedName("UnitDetails")
    @Expose
    private UnitDetails unitDetails;
    @SerializedName("WarrantyDetails")
    @Expose
    private WarrantyDetails warrantyDetails;
    @SerializedName("FaultReport")
    @Expose
    private FaultReport faultReport;
    @SerializedName("PartsAttached")
    @Expose
    private List<Object> partsAttached = null;
    @SerializedName("ServiceHistory")
    @Expose
    private ServiceHistory serviceHistory;
    @SerializedName("JobAttachments")
    @Expose
    private List<Object> jobAttachments = null;
    @SerializedName("ContactHistories")
    @Expose
    private List<ContactHistory> contactHistories = null;

    public String getAdditionalEngineerID() {
        return additionalEngineerID;
    }

    public void setAdditionalEngineerID(String additionalEngineerID) {
        this.additionalEngineerID = additionalEngineerID;
    }

    public String getSLAppID() {
        return sLAppID;
    }

    public void setSLAppID(String sLAppID) {
        this.sLAppID = sLAppID;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getServiceProviderID() {
        return serviceProviderID;
    }

    public void setServiceProviderID(String serviceProviderID) {
        this.serviceProviderID = serviceProviderID;
    }

    public String getSendCustomerReceiptEmail() {
        return sendCustomerReceiptEmail;
    }

    public void setSendCustomerReceiptEmail(String sendCustomerReceiptEmail) {
        this.sendCustomerReceiptEmail = sendCustomerReceiptEmail;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public AppointmentDefaults getAppointmentDefaults() {
        return appointmentDefaults;
    }

    public void setAppointmentDefaults(AppointmentDefaults appointmentDefaults) {
        this.appointmentDefaults = appointmentDefaults;
    }

    public JobDetails getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(JobDetails jobDetails) {
        this.jobDetails = jobDetails;
    }

    public List<Object> getJobFaultCodes() {
        return jobFaultCodes;
    }

    public void setJobFaultCodes(List<Object> jobFaultCodes) {
        this.jobFaultCodes = jobFaultCodes;
    }

    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    public UnitDetails getUnitDetails() {
        return unitDetails;
    }

    public void setUnitDetails(UnitDetails unitDetails) {
        this.unitDetails = unitDetails;
    }

    public WarrantyDetails getWarrantyDetails() {
        return warrantyDetails;
    }

    public void setWarrantyDetails(WarrantyDetails warrantyDetails) {
        this.warrantyDetails = warrantyDetails;
    }

    public FaultReport getFaultReport() {
        return faultReport;
    }

    public void setFaultReport(FaultReport faultReport) {
        this.faultReport = faultReport;
    }

    public List<Object> getPartsAttached() {
        return partsAttached;
    }

    public void setPartsAttached(List<Object> partsAttached) {
        this.partsAttached = partsAttached;
    }

    public ServiceHistory getServiceHistory() {
        return serviceHistory;
    }

    public void setServiceHistory(ServiceHistory serviceHistory) {
        this.serviceHistory = serviceHistory;
    }

    public List<Object> getJobAttachments() {
        return jobAttachments;
    }

    public void setJobAttachments(List<Object> jobAttachments) {
        this.jobAttachments = jobAttachments;
    }

    public List<ContactHistory> getContactHistories() {
        return contactHistories;
    }

    public void setContactHistories(List<ContactHistory> contactHistories) {
        this.contactHistories = contactHistories;
    }

}
