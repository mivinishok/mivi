
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "OutCardLeft",
    "CustomerDetailsChecked",
    "UnitDetailsChecked",
    "WarrantyDetailsChecked",
    "CloseVisitAllowed",
    "AppointmentCompleted",
    "CompletionStatusRequired",
    "ConfirmedByUser",
    "Cancelled"
})
public class AppointmentDefaults {

    @JsonProperty("OutCardLeft")
    private String outCardLeft;
    @JsonProperty("CustomerDetailsChecked")
    private String customerDetailsChecked;
    @JsonProperty("UnitDetailsChecked")
    private String unitDetailsChecked;
    @JsonProperty("WarrantyDetailsChecked")
    private String warrantyDetailsChecked;
    @JsonProperty("CloseVisitAllowed")
    private String closeVisitAllowed;
    @JsonProperty("AppointmentCompleted")
    private String appointmentCompleted;
    @JsonProperty("CompletionStatusRequired")
    private String completionStatusRequired;
    @JsonProperty("ConfirmedByUser")
    private String confirmedByUser;
    @JsonProperty("Cancelled")
    private String cancelled;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("OutCardLeft")
    public String getOutCardLeft() {
        return outCardLeft;
    }

    @JsonProperty("OutCardLeft")
    public void setOutCardLeft(String outCardLeft) {
        this.outCardLeft = outCardLeft;
    }

    @JsonProperty("CustomerDetailsChecked")
    public String getCustomerDetailsChecked() {
        return customerDetailsChecked;
    }

    @JsonProperty("CustomerDetailsChecked")
    public void setCustomerDetailsChecked(String customerDetailsChecked) {
        this.customerDetailsChecked = customerDetailsChecked;
    }

    @JsonProperty("UnitDetailsChecked")
    public String getUnitDetailsChecked() {
        return unitDetailsChecked;
    }

    @JsonProperty("UnitDetailsChecked")
    public void setUnitDetailsChecked(String unitDetailsChecked) {
        this.unitDetailsChecked = unitDetailsChecked;
    }

    @JsonProperty("WarrantyDetailsChecked")
    public String getWarrantyDetailsChecked() {
        return warrantyDetailsChecked;
    }

    @JsonProperty("WarrantyDetailsChecked")
    public void setWarrantyDetailsChecked(String warrantyDetailsChecked) {
        this.warrantyDetailsChecked = warrantyDetailsChecked;
    }

    @JsonProperty("CloseVisitAllowed")
    public String getCloseVisitAllowed() {
        return closeVisitAllowed;
    }

    @JsonProperty("CloseVisitAllowed")
    public void setCloseVisitAllowed(String closeVisitAllowed) {
        this.closeVisitAllowed = closeVisitAllowed;
    }

    @JsonProperty("AppointmentCompleted")
    public String getAppointmentCompleted() {
        return appointmentCompleted;
    }

    @JsonProperty("AppointmentCompleted")
    public void setAppointmentCompleted(String appointmentCompleted) {
        this.appointmentCompleted = appointmentCompleted;
    }

    @JsonProperty("CompletionStatusRequired")
    public String getCompletionStatusRequired() {
        return completionStatusRequired;
    }

    @JsonProperty("CompletionStatusRequired")
    public void setCompletionStatusRequired(String completionStatusRequired) {
        this.completionStatusRequired = completionStatusRequired;
    }

    @JsonProperty("ConfirmedByUser")
    public String getConfirmedByUser() {
        return confirmedByUser;
    }

    @JsonProperty("ConfirmedByUser")
    public void setConfirmedByUser(String confirmedByUser) {
        this.confirmedByUser = confirmedByUser;
    }

    @JsonProperty("Cancelled")
    public String getCancelled() {
        return cancelled;
    }

    @JsonProperty("Cancelled")
    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
