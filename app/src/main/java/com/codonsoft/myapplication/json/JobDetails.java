
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "JobNo",
    "SLJobNo",
    "ArgosSparesManagement",
    "EstimateRequired",
    "EstimateIfOver",
    "EstimateAccepted",
    "EstimateRejected",
    "AppointmentNotes",
    "WarrantyJob",
    "JobTypeID",
    "PreviousVisits",
    "DisplayChargeableCosts",
    "ChargeableLabourCost",
    "ChargeablePartsCost",
    "CollectionDeliveryCost",
    "ChargeableVATCost",
    "ChargeableTotalCost",
    "ChargeableTotalPaid",
    "ChargeableRefund",
    "ChargableBalanceDue",
    "JobCostID",
    "ChargeableLabourVATCost",
    "ChargeableSubTotal",
    "ChargeablePriceLock",
    "ChargeableNetworkManagerOverride",
    "ChargeableComments",
    "ChargeableApproved",
    "ChargeableModifiedBy",
    "AppointmentWindow",
    "Duration",
    "Network",
    "NetworkID",
    "Client",
    "ClientID",
    "Branch",
    "BranchID",
    "BranchOpenTime",
    "BranchCloseTime",
    "RepeatRepair",
    "ServiceTypeID1",
    "ServiceTypeName1",
    "ServiceTypeIDs",
    "UnitTypeID",
    "RepairSkillID"
})
public class JobDetails {

    @JsonProperty("JobNo")
    private String jobNo;
    @JsonProperty("SLJobNo")
    private String sLJobNo;
    @JsonProperty("ArgosSparesManagement")
    private String argosSparesManagement;
    @JsonProperty("EstimateRequired")
    private String estimateRequired;
    @JsonProperty("EstimateIfOver")
    private String estimateIfOver;
    @JsonProperty("EstimateAccepted")
    private String estimateAccepted;
    @JsonProperty("EstimateRejected")
    private String estimateRejected;
    @JsonProperty("AppointmentNotes")
    private String appointmentNotes;
    @JsonProperty("WarrantyJob")
    private String warrantyJob;
    @JsonProperty("JobTypeID")
    private String jobTypeID;
    @JsonProperty("PreviousVisits")
    private String previousVisits;
    @JsonProperty("DisplayChargeableCosts")
    private String displayChargeableCosts;
    @JsonProperty("ChargeableLabourCost")
    private String chargeableLabourCost;
    @JsonProperty("ChargeablePartsCost")
    private String chargeablePartsCost;
    @JsonProperty("CollectionDeliveryCost")
    private String collectionDeliveryCost;
    @JsonProperty("ChargeableVATCost")
    private String chargeableVATCost;
    @JsonProperty("ChargeableTotalCost")
    private String chargeableTotalCost;
    @JsonProperty("ChargeableTotalPaid")
    private String chargeableTotalPaid;
    @JsonProperty("ChargeableRefund")
    private String chargeableRefund;
    @JsonProperty("ChargableBalanceDue")
    private String chargableBalanceDue;
    @JsonProperty("JobCostID")
    private String jobCostID;
    @JsonProperty("ChargeableLabourVATCost")
    private String chargeableLabourVATCost;
    @JsonProperty("ChargeableSubTotal")
    private String chargeableSubTotal;
    @JsonProperty("ChargeablePriceLock")
    private String chargeablePriceLock;
    @JsonProperty("ChargeableNetworkManagerOverride")
    private String chargeableNetworkManagerOverride;
    @JsonProperty("ChargeableComments")
    private String chargeableComments;
    @JsonProperty("ChargeableApproved")
    private String chargeableApproved;
    @JsonProperty("ChargeableModifiedBy")
    private String chargeableModifiedBy;
    @JsonProperty("AppointmentWindow")
    private String appointmentWindow;
    @JsonProperty("Duration")
    private String duration;
    @JsonProperty("Network")
    private String network;
    @JsonProperty("NetworkID")
    private String networkID;
    @JsonProperty("Client")
    private String client;
    @JsonProperty("ClientID")
    private String clientID;
    @JsonProperty("Branch")
    private String branch;
    @JsonProperty("BranchID")
    private String branchID;
    @JsonProperty("BranchOpenTime")
    private String branchOpenTime;
    @JsonProperty("BranchCloseTime")
    private String branchCloseTime;
    @JsonProperty("RepeatRepair")
    private String repeatRepair;
    @JsonProperty("ServiceTypeID1")
    private String serviceTypeID1;
    @JsonProperty("ServiceTypeName1")
    private String serviceTypeName1;
    @JsonProperty("ServiceTypeIDs")
    private ServiceTypeIDs serviceTypeIDs;
    @JsonProperty("UnitTypeID")
    private String unitTypeID;
    @JsonProperty("RepairSkillID")
    private String repairSkillID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("JobNo")
    public String getJobNo() {
        return jobNo;
    }

    @JsonProperty("JobNo")
    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    @JsonProperty("SLJobNo")
    public String getSLJobNo() {
        return sLJobNo;
    }

    @JsonProperty("SLJobNo")
    public void setSLJobNo(String sLJobNo) {
        this.sLJobNo = sLJobNo;
    }

    @JsonProperty("ArgosSparesManagement")
    public String getArgosSparesManagement() {
        return argosSparesManagement;
    }

    @JsonProperty("ArgosSparesManagement")
    public void setArgosSparesManagement(String argosSparesManagement) {
        this.argosSparesManagement = argosSparesManagement;
    }

    @JsonProperty("EstimateRequired")
    public String getEstimateRequired() {
        return estimateRequired;
    }

    @JsonProperty("EstimateRequired")
    public void setEstimateRequired(String estimateRequired) {
        this.estimateRequired = estimateRequired;
    }

    @JsonProperty("EstimateIfOver")
    public String getEstimateIfOver() {
        return estimateIfOver;
    }

    @JsonProperty("EstimateIfOver")
    public void setEstimateIfOver(String estimateIfOver) {
        this.estimateIfOver = estimateIfOver;
    }

    @JsonProperty("EstimateAccepted")
    public String getEstimateAccepted() {
        return estimateAccepted;
    }

    @JsonProperty("EstimateAccepted")
    public void setEstimateAccepted(String estimateAccepted) {
        this.estimateAccepted = estimateAccepted;
    }

    @JsonProperty("EstimateRejected")
    public String getEstimateRejected() {
        return estimateRejected;
    }

    @JsonProperty("EstimateRejected")
    public void setEstimateRejected(String estimateRejected) {
        this.estimateRejected = estimateRejected;
    }

    @JsonProperty("AppointmentNotes")
    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    @JsonProperty("AppointmentNotes")
    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }

    @JsonProperty("WarrantyJob")
    public String getWarrantyJob() {
        return warrantyJob;
    }

    @JsonProperty("WarrantyJob")
    public void setWarrantyJob(String warrantyJob) {
        this.warrantyJob = warrantyJob;
    }

    @JsonProperty("JobTypeID")
    public String getJobTypeID() {
        return jobTypeID;
    }

    @JsonProperty("JobTypeID")
    public void setJobTypeID(String jobTypeID) {
        this.jobTypeID = jobTypeID;
    }

    @JsonProperty("PreviousVisits")
    public String getPreviousVisits() {
        return previousVisits;
    }

    @JsonProperty("PreviousVisits")
    public void setPreviousVisits(String previousVisits) {
        this.previousVisits = previousVisits;
    }

    @JsonProperty("DisplayChargeableCosts")
    public String getDisplayChargeableCosts() {
        return displayChargeableCosts;
    }

    @JsonProperty("DisplayChargeableCosts")
    public void setDisplayChargeableCosts(String displayChargeableCosts) {
        this.displayChargeableCosts = displayChargeableCosts;
    }

    @JsonProperty("ChargeableLabourCost")
    public String getChargeableLabourCost() {
        return chargeableLabourCost;
    }

    @JsonProperty("ChargeableLabourCost")
    public void setChargeableLabourCost(String chargeableLabourCost) {
        this.chargeableLabourCost = chargeableLabourCost;
    }

    @JsonProperty("ChargeablePartsCost")
    public String getChargeablePartsCost() {
        return chargeablePartsCost;
    }

    @JsonProperty("ChargeablePartsCost")
    public void setChargeablePartsCost(String chargeablePartsCost) {
        this.chargeablePartsCost = chargeablePartsCost;
    }

    @JsonProperty("CollectionDeliveryCost")
    public String getCollectionDeliveryCost() {
        return collectionDeliveryCost;
    }

    @JsonProperty("CollectionDeliveryCost")
    public void setCollectionDeliveryCost(String collectionDeliveryCost) {
        this.collectionDeliveryCost = collectionDeliveryCost;
    }

    @JsonProperty("ChargeableVATCost")
    public String getChargeableVATCost() {
        return chargeableVATCost;
    }

    @JsonProperty("ChargeableVATCost")
    public void setChargeableVATCost(String chargeableVATCost) {
        this.chargeableVATCost = chargeableVATCost;
    }

    @JsonProperty("ChargeableTotalCost")
    public String getChargeableTotalCost() {
        return chargeableTotalCost;
    }

    @JsonProperty("ChargeableTotalCost")
    public void setChargeableTotalCost(String chargeableTotalCost) {
        this.chargeableTotalCost = chargeableTotalCost;
    }

    @JsonProperty("ChargeableTotalPaid")
    public String getChargeableTotalPaid() {
        return chargeableTotalPaid;
    }

    @JsonProperty("ChargeableTotalPaid")
    public void setChargeableTotalPaid(String chargeableTotalPaid) {
        this.chargeableTotalPaid = chargeableTotalPaid;
    }

    @JsonProperty("ChargeableRefund")
    public String getChargeableRefund() {
        return chargeableRefund;
    }

    @JsonProperty("ChargeableRefund")
    public void setChargeableRefund(String chargeableRefund) {
        this.chargeableRefund = chargeableRefund;
    }

    @JsonProperty("ChargableBalanceDue")
    public String getChargableBalanceDue() {
        return chargableBalanceDue;
    }

    @JsonProperty("ChargableBalanceDue")
    public void setChargableBalanceDue(String chargableBalanceDue) {
        this.chargableBalanceDue = chargableBalanceDue;
    }

    @JsonProperty("JobCostID")
    public String getJobCostID() {
        return jobCostID;
    }

    @JsonProperty("JobCostID")
    public void setJobCostID(String jobCostID) {
        this.jobCostID = jobCostID;
    }

    @JsonProperty("ChargeableLabourVATCost")
    public String getChargeableLabourVATCost() {
        return chargeableLabourVATCost;
    }

    @JsonProperty("ChargeableLabourVATCost")
    public void setChargeableLabourVATCost(String chargeableLabourVATCost) {
        this.chargeableLabourVATCost = chargeableLabourVATCost;
    }

    @JsonProperty("ChargeableSubTotal")
    public String getChargeableSubTotal() {
        return chargeableSubTotal;
    }

    @JsonProperty("ChargeableSubTotal")
    public void setChargeableSubTotal(String chargeableSubTotal) {
        this.chargeableSubTotal = chargeableSubTotal;
    }

    @JsonProperty("ChargeablePriceLock")
    public String getChargeablePriceLock() {
        return chargeablePriceLock;
    }

    @JsonProperty("ChargeablePriceLock")
    public void setChargeablePriceLock(String chargeablePriceLock) {
        this.chargeablePriceLock = chargeablePriceLock;
    }

    @JsonProperty("ChargeableNetworkManagerOverride")
    public String getChargeableNetworkManagerOverride() {
        return chargeableNetworkManagerOverride;
    }

    @JsonProperty("ChargeableNetworkManagerOverride")
    public void setChargeableNetworkManagerOverride(String chargeableNetworkManagerOverride) {
        this.chargeableNetworkManagerOverride = chargeableNetworkManagerOverride;
    }

    @JsonProperty("ChargeableComments")
    public String getChargeableComments() {
        return chargeableComments;
    }

    @JsonProperty("ChargeableComments")
    public void setChargeableComments(String chargeableComments) {
        this.chargeableComments = chargeableComments;
    }

    @JsonProperty("ChargeableApproved")
    public String getChargeableApproved() {
        return chargeableApproved;
    }

    @JsonProperty("ChargeableApproved")
    public void setChargeableApproved(String chargeableApproved) {
        this.chargeableApproved = chargeableApproved;
    }

    @JsonProperty("ChargeableModifiedBy")
    public String getChargeableModifiedBy() {
        return chargeableModifiedBy;
    }

    @JsonProperty("ChargeableModifiedBy")
    public void setChargeableModifiedBy(String chargeableModifiedBy) {
        this.chargeableModifiedBy = chargeableModifiedBy;
    }

    @JsonProperty("AppointmentWindow")
    public String getAppointmentWindow() {
        return appointmentWindow;
    }

    @JsonProperty("AppointmentWindow")
    public void setAppointmentWindow(String appointmentWindow) {
        this.appointmentWindow = appointmentWindow;
    }

    @JsonProperty("Duration")
    public String getDuration() {
        return duration;
    }

    @JsonProperty("Duration")
    public void setDuration(String duration) {
        this.duration = duration;
    }

    @JsonProperty("Network")
    public String getNetwork() {
        return network;
    }

    @JsonProperty("Network")
    public void setNetwork(String network) {
        this.network = network;
    }

    @JsonProperty("NetworkID")
    public String getNetworkID() {
        return networkID;
    }

    @JsonProperty("NetworkID")
    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    @JsonProperty("Client")
    public String getClient() {
        return client;
    }

    @JsonProperty("Client")
    public void setClient(String client) {
        this.client = client;
    }

    @JsonProperty("ClientID")
    public String getClientID() {
        return clientID;
    }

    @JsonProperty("ClientID")
    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    @JsonProperty("Branch")
    public String getBranch() {
        return branch;
    }

    @JsonProperty("Branch")
    public void setBranch(String branch) {
        this.branch = branch;
    }

    @JsonProperty("BranchID")
    public String getBranchID() {
        return branchID;
    }

    @JsonProperty("BranchID")
    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    @JsonProperty("BranchOpenTime")
    public String getBranchOpenTime() {
        return branchOpenTime;
    }

    @JsonProperty("BranchOpenTime")
    public void setBranchOpenTime(String branchOpenTime) {
        this.branchOpenTime = branchOpenTime;
    }

    @JsonProperty("BranchCloseTime")
    public String getBranchCloseTime() {
        return branchCloseTime;
    }

    @JsonProperty("BranchCloseTime")
    public void setBranchCloseTime(String branchCloseTime) {
        this.branchCloseTime = branchCloseTime;
    }

    @JsonProperty("RepeatRepair")
    public String getRepeatRepair() {
        return repeatRepair;
    }

    @JsonProperty("RepeatRepair")
    public void setRepeatRepair(String repeatRepair) {
        this.repeatRepair = repeatRepair;
    }

    @JsonProperty("ServiceTypeID1")
    public String getServiceTypeID1() {
        return serviceTypeID1;
    }

    @JsonProperty("ServiceTypeID1")
    public void setServiceTypeID1(String serviceTypeID1) {
        this.serviceTypeID1 = serviceTypeID1;
    }

    @JsonProperty("ServiceTypeName1")
    public String getServiceTypeName1() {
        return serviceTypeName1;
    }

    @JsonProperty("ServiceTypeName1")
    public void setServiceTypeName1(String serviceTypeName1) {
        this.serviceTypeName1 = serviceTypeName1;
    }

    @JsonProperty("ServiceTypeIDs")
    public ServiceTypeIDs getServiceTypeIDs() {
        return serviceTypeIDs;
    }

    @JsonProperty("ServiceTypeIDs")
    public void setServiceTypeIDs(ServiceTypeIDs serviceTypeIDs) {
        this.serviceTypeIDs = serviceTypeIDs;
    }

    @JsonProperty("UnitTypeID")
    public String getUnitTypeID() {
        return unitTypeID;
    }

    @JsonProperty("UnitTypeID")
    public void setUnitTypeID(String unitTypeID) {
        this.unitTypeID = unitTypeID;
    }

    @JsonProperty("RepairSkillID")
    public String getRepairSkillID() {
        return repairSkillID;
    }

    @JsonProperty("RepairSkillID")
    public void setRepairSkillID(String repairSkillID) {
        this.repairSkillID = repairSkillID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
