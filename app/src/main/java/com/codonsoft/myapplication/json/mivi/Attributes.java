
package com.codonsoft.myapplication.json.mivi;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "payment-type",
    "unbilled-charges",
    "next-billing-date",
    "title",
    "first-name",
    "last-name",
    "date-of-birth",
    "contact-number",
    "email-address",
    "email-address-verified",
    "email-subscription-status"
})
public class Attributes {

    @JsonProperty("payment-type")
    private String paymentType;
    @JsonProperty("unbilled-charges")
    private Object unbilledCharges;
    @JsonProperty("next-billing-date")
    private Object nextBillingDate;
    @JsonProperty("title")
    private String title;
    @JsonProperty("first-name")
    private String firstName;
    @JsonProperty("last-name")
    private String lastName;
    @JsonProperty("date-of-birth")
    private String dateOfBirth;
    @JsonProperty("contact-number")
    private String contactNumber;
    @JsonProperty("email-address")
    private String emailAddress;
    @JsonProperty("email-address-verified")
    private Boolean emailAddressVerified;
    @JsonProperty("email-subscription-status")
    private Boolean emailSubscriptionStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("payment-type")
    public String getPaymentType() {
        return paymentType;
    }

    @JsonProperty("payment-type")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    @JsonProperty("unbilled-charges")
    public Object getUnbilledCharges() {
        return unbilledCharges;
    }

    @JsonProperty("unbilled-charges")
    public void setUnbilledCharges(Object unbilledCharges) {
        this.unbilledCharges = unbilledCharges;
    }

    @JsonProperty("next-billing-date")
    public Object getNextBillingDate() {
        return nextBillingDate;
    }

    @JsonProperty("next-billing-date")
    public void setNextBillingDate(Object nextBillingDate) {
        this.nextBillingDate = nextBillingDate;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("first-name")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("first-name")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("last-name")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("last-name")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("date-of-birth")
    public String getDateOfBirth() {
        return dateOfBirth;
    }

    @JsonProperty("date-of-birth")
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @JsonProperty("contact-number")
    public String getContactNumber() {
        return contactNumber;
    }

    @JsonProperty("contact-number")
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @JsonProperty("email-address")
    public String getEmailAddress() {
        return emailAddress;
    }

    @JsonProperty("email-address")
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @JsonProperty("email-address-verified")
    public Boolean getEmailAddressVerified() {
        return emailAddressVerified;
    }

    @JsonProperty("email-address-verified")
    public void setEmailAddressVerified(Boolean emailAddressVerified) {
        this.emailAddressVerified = emailAddressVerified;
    }

    @JsonProperty("email-subscription-status")
    public Boolean getEmailSubscriptionStatus() {
        return emailSubscriptionStatus;
    }

    @JsonProperty("email-subscription-status")
    public void setEmailSubscriptionStatus(Boolean emailSubscriptionStatus) {
        this.emailSubscriptionStatus = emailSubscriptionStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
