
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactHistory {

    @SerializedName("Date")
    @Expose
    private String date;
    @SerializedName("Time")
    @Expose
    private String time;
    @SerializedName("User")
    @Expose
    private String user;
    @SerializedName("AppointmentID")
    @Expose
    private List<Object> appointmentID = null;
    @SerializedName("Action")
    @Expose
    private String action;
    @SerializedName("Notes")
    @Expose
    private String notes;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public List<Object> getAppointmentID() {
        return appointmentID;
    }

    public void setAppointmentID(List<Object> appointmentID) {
        this.appointmentID = appointmentID;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
