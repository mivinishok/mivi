
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppointmentDefaults {

    @SerializedName("OutCardLeft")
    @Expose
    private String outCardLeft;
    @SerializedName("CustomerDetailsChecked")
    @Expose
    private String customerDetailsChecked;
    @SerializedName("UnitDetailsChecked")
    @Expose
    private String unitDetailsChecked;
    @SerializedName("WarrantyDetailsChecked")
    @Expose
    private String warrantyDetailsChecked;
    @SerializedName("CloseVisitAllowed")
    @Expose
    private String closeVisitAllowed;
    @SerializedName("AppointmentCompleted")
    @Expose
    private String appointmentCompleted;
    @SerializedName("CompletionStatusRequired")
    @Expose
    private String completionStatusRequired;
    @SerializedName("ConfirmedByUser")
    @Expose
    private String confirmedByUser;
    @SerializedName("ConfirmedByUserDate")
    @Expose
    private List<Object> confirmedByUserDate = null;
    @SerializedName("Cancelled")
    @Expose
    private String cancelled;

    public String getOutCardLeft() {
        return outCardLeft;
    }

    public void setOutCardLeft(String outCardLeft) {
        this.outCardLeft = outCardLeft;
    }

    public String getCustomerDetailsChecked() {
        return customerDetailsChecked;
    }

    public void setCustomerDetailsChecked(String customerDetailsChecked) {
        this.customerDetailsChecked = customerDetailsChecked;
    }

    public String getUnitDetailsChecked() {
        return unitDetailsChecked;
    }

    public void setUnitDetailsChecked(String unitDetailsChecked) {
        this.unitDetailsChecked = unitDetailsChecked;
    }

    public String getWarrantyDetailsChecked() {
        return warrantyDetailsChecked;
    }

    public void setWarrantyDetailsChecked(String warrantyDetailsChecked) {
        this.warrantyDetailsChecked = warrantyDetailsChecked;
    }

    public String getCloseVisitAllowed() {
        return closeVisitAllowed;
    }

    public void setCloseVisitAllowed(String closeVisitAllowed) {
        this.closeVisitAllowed = closeVisitAllowed;
    }

    public String getAppointmentCompleted() {
        return appointmentCompleted;
    }

    public void setAppointmentCompleted(String appointmentCompleted) {
        this.appointmentCompleted = appointmentCompleted;
    }

    public String getCompletionStatusRequired() {
        return completionStatusRequired;
    }

    public void setCompletionStatusRequired(String completionStatusRequired) {
        this.completionStatusRequired = completionStatusRequired;
    }

    public String getConfirmedByUser() {
        return confirmedByUser;
    }

    public void setConfirmedByUser(String confirmedByUser) {
        this.confirmedByUser = confirmedByUser;
    }

    public List<Object> getConfirmedByUserDate() {
        return confirmedByUserDate;
    }

    public void setConfirmedByUserDate(List<Object> confirmedByUserDate) {
        this.confirmedByUserDate = confirmedByUserDate;
    }

    public String getCancelled() {
        return cancelled;
    }

    public void setCancelled(String cancelled) {
        this.cancelled = cancelled;
    }

}
