
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceHistory {

    @SerializedName("DescriptionOfRepair")
    @Expose
    private List<Object> descriptionOfRepair = null;

    public List<Object> getDescriptionOfRepair() {
        return descriptionOfRepair;
    }

    public void setDescriptionOfRepair(List<Object> descriptionOfRepair) {
        this.descriptionOfRepair = descriptionOfRepair;
    }

}
