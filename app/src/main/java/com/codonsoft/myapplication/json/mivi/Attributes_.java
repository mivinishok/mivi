
package com.codonsoft.myapplication.json.mivi;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "msn",
    "credit",
    "credit-expiry",
    "data-usage-threshold",
    "included-data-balance",
    "included-credit-balance",
    "included-rollover-credit-balance",
    "included-rollover-data-balance",
    "included-international-talk-balance",
    "expiry-date",
    "auto-renewal",
    "primary-subscription",
    "name",
    "included-data",
    "included-credit",
    "included-international-talk",
    "unlimited-text",
    "unlimited-talk",
    "unlimited-international-text",
    "unlimited-international-talk",
    "price"
})
public class Attributes_ {

    @JsonProperty("msn")
    private String msn;
    @JsonProperty("credit")
    private Integer credit;
    @JsonProperty("credit-expiry")
    private String creditExpiry;
    @JsonProperty("data-usage-threshold")
    private Boolean dataUsageThreshold;
    @JsonProperty("included-data-balance")
    private Integer includedDataBalance;
    @JsonProperty("included-credit-balance")
    private Object includedCreditBalance;
    @JsonProperty("included-rollover-credit-balance")
    private Object includedRolloverCreditBalance;
    @JsonProperty("included-rollover-data-balance")
    private Object includedRolloverDataBalance;
    @JsonProperty("included-international-talk-balance")
    private Object includedInternationalTalkBalance;
    @JsonProperty("expiry-date")
    private String expiryDate;
    @JsonProperty("auto-renewal")
    private Boolean autoRenewal;
    @JsonProperty("primary-subscription")
    private Boolean primarySubscription;
    @JsonProperty("name")
    private String name;
    @JsonProperty("included-data")
    private Object includedData;
    @JsonProperty("included-credit")
    private Object includedCredit;
    @JsonProperty("included-international-talk")
    private Object includedInternationalTalk;
    @JsonProperty("unlimited-text")
    private Boolean unlimitedText;
    @JsonProperty("unlimited-talk")
    private Boolean unlimitedTalk;
    @JsonProperty("unlimited-international-text")
    private Boolean unlimitedInternationalText;
    @JsonProperty("unlimited-international-talk")
    private Boolean unlimitedInternationalTalk;
    @JsonProperty("price")
    private Integer price;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("msn")
    public String getMsn() {
        return msn;
    }

    @JsonProperty("msn")
    public void setMsn(String msn) {
        this.msn = msn;
    }

    @JsonProperty("credit")
    public Integer getCredit() {
        return credit;
    }

    @JsonProperty("credit")
    public void setCredit(Integer credit) {
        this.credit = credit;
    }

    @JsonProperty("credit-expiry")
    public String getCreditExpiry() {
        return creditExpiry;
    }

    @JsonProperty("credit-expiry")
    public void setCreditExpiry(String creditExpiry) {
        this.creditExpiry = creditExpiry;
    }

    @JsonProperty("data-usage-threshold")
    public Boolean getDataUsageThreshold() {
        return dataUsageThreshold;
    }

    @JsonProperty("data-usage-threshold")
    public void setDataUsageThreshold(Boolean dataUsageThreshold) {
        this.dataUsageThreshold = dataUsageThreshold;
    }

    @JsonProperty("included-data-balance")
    public Integer getIncludedDataBalance() {
        return includedDataBalance;
    }

    @JsonProperty("included-data-balance")
    public void setIncludedDataBalance(Integer includedDataBalance) {
        this.includedDataBalance = includedDataBalance;
    }

    @JsonProperty("included-credit-balance")
    public Object getIncludedCreditBalance() {
        return includedCreditBalance;
    }

    @JsonProperty("included-credit-balance")
    public void setIncludedCreditBalance(Object includedCreditBalance) {
        this.includedCreditBalance = includedCreditBalance;
    }

    @JsonProperty("included-rollover-credit-balance")
    public Object getIncludedRolloverCreditBalance() {
        return includedRolloverCreditBalance;
    }

    @JsonProperty("included-rollover-credit-balance")
    public void setIncludedRolloverCreditBalance(Object includedRolloverCreditBalance) {
        this.includedRolloverCreditBalance = includedRolloverCreditBalance;
    }

    @JsonProperty("included-rollover-data-balance")
    public Object getIncludedRolloverDataBalance() {
        return includedRolloverDataBalance;
    }

    @JsonProperty("included-rollover-data-balance")
    public void setIncludedRolloverDataBalance(Object includedRolloverDataBalance) {
        this.includedRolloverDataBalance = includedRolloverDataBalance;
    }

    @JsonProperty("included-international-talk-balance")
    public Object getIncludedInternationalTalkBalance() {
        return includedInternationalTalkBalance;
    }

    @JsonProperty("included-international-talk-balance")
    public void setIncludedInternationalTalkBalance(Object includedInternationalTalkBalance) {
        this.includedInternationalTalkBalance = includedInternationalTalkBalance;
    }

    @JsonProperty("expiry-date")
    public String getExpiryDate() {
        return expiryDate;
    }

    @JsonProperty("expiry-date")
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @JsonProperty("auto-renewal")
    public Boolean getAutoRenewal() {
        return autoRenewal;
    }

    @JsonProperty("auto-renewal")
    public void setAutoRenewal(Boolean autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    @JsonProperty("primary-subscription")
    public Boolean getPrimarySubscription() {
        return primarySubscription;
    }

    @JsonProperty("primary-subscription")
    public void setPrimarySubscription(Boolean primarySubscription) {
        this.primarySubscription = primarySubscription;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("included-data")
    public Object getIncludedData() {
        return includedData;
    }

    @JsonProperty("included-data")
    public void setIncludedData(Object includedData) {
        this.includedData = includedData;
    }

    @JsonProperty("included-credit")
    public Object getIncludedCredit() {
        return includedCredit;
    }

    @JsonProperty("included-credit")
    public void setIncludedCredit(Object includedCredit) {
        this.includedCredit = includedCredit;
    }

    @JsonProperty("included-international-talk")
    public Object getIncludedInternationalTalk() {
        return includedInternationalTalk;
    }

    @JsonProperty("included-international-talk")
    public void setIncludedInternationalTalk(Object includedInternationalTalk) {
        this.includedInternationalTalk = includedInternationalTalk;
    }

    @JsonProperty("unlimited-text")
    public Boolean getUnlimitedText() {
        return unlimitedText;
    }

    @JsonProperty("unlimited-text")
    public void setUnlimitedText(Boolean unlimitedText) {
        this.unlimitedText = unlimitedText;
    }

    @JsonProperty("unlimited-talk")
    public Boolean getUnlimitedTalk() {
        return unlimitedTalk;
    }

    @JsonProperty("unlimited-talk")
    public void setUnlimitedTalk(Boolean unlimitedTalk) {
        this.unlimitedTalk = unlimitedTalk;
    }

    @JsonProperty("unlimited-international-text")
    public Boolean getUnlimitedInternationalText() {
        return unlimitedInternationalText;
    }

    @JsonProperty("unlimited-international-text")
    public void setUnlimitedInternationalText(Boolean unlimitedInternationalText) {
        this.unlimitedInternationalText = unlimitedInternationalText;
    }

    @JsonProperty("unlimited-international-talk")
    public Boolean getUnlimitedInternationalTalk() {
        return unlimitedInternationalTalk;
    }

    @JsonProperty("unlimited-international-talk")
    public void setUnlimitedInternationalTalk(Boolean unlimitedInternationalTalk) {
        this.unlimitedInternationalTalk = unlimitedInternationalTalk;
    }

    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
