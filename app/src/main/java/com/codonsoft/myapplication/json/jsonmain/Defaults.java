
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Defaults {

    @SerializedName("FullName")
    @Expose
    private String fullName;
    @SerializedName("StartTime")
    @Expose
    private String startTime;
    @SerializedName("FinishTime")
    @Expose
    private String finishTime;
    @SerializedName("SLUsername")
    @Expose
    private String sLUsername;
    @SerializedName("SLPassword")
    @Expose
    private String sLPassword;
    @SerializedName("DisclaimerText")
    @Expose
    private List<Object> disclaimerText = null;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    public String getSLUsername() {
        return sLUsername;
    }

    public void setSLUsername(String sLUsername) {
        this.sLUsername = sLUsername;
    }

    public String getSLPassword() {
        return sLPassword;
    }

    public void setSLPassword(String sLPassword) {
        this.sLPassword = sLPassword;
    }

    public List<Object> getDisclaimerText() {
        return disclaimerText;
    }

    public void setDisclaimerText(List<Object> disclaimerText) {
        this.disclaimerText = disclaimerText;
    }

}
