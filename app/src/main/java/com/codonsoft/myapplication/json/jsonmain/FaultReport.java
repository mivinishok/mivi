
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaultReport {

    @SerializedName("FaultDescription")
    @Expose
    private String faultDescription;
    @SerializedName("FaultHistories")
    @Expose
    private List<Object> faultHistories = null;

    public String getFaultDescription() {
        return faultDescription;
    }

    public void setFaultDescription(String faultDescription) {
        this.faultDescription = faultDescription;
    }

    public List<Object> getFaultHistories() {
        return faultHistories;
    }

    public void setFaultHistories(List<Object> faultHistories) {
        this.faultHistories = faultHistories;
    }

}
