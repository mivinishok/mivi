
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "AdditionalEngineerID",
    "SLAppID",
    "SortOrder",
    "AppointmentType",
    "ServiceProviderID",
    "SendCustomerReceiptEmail",
    "TimeSlot",
    "AppointmentDefaults",
    "JobDetails",
    "CustomerDetails",
    "UnitDetails",
    "WarrantyDetails",
    "FaultReport",
    "ServiceHistory",
    "ContactHistories"
})
public class Appointment {

    @JsonProperty("AdditionalEngineerID")
    private String additionalEngineerID;
    @JsonProperty("SLAppID")
    private String sLAppID;
    @JsonProperty("SortOrder")
    private String sortOrder;
    @JsonProperty("AppointmentType")
    private String appointmentType;
    @JsonProperty("ServiceProviderID")
    private String serviceProviderID;
    @JsonProperty("SendCustomerReceiptEmail")
    private String sendCustomerReceiptEmail;
    @JsonProperty("TimeSlot")
    private String timeSlot;
    @JsonProperty("AppointmentDefaults")
    private AppointmentDefaults appointmentDefaults;
    @JsonProperty("JobDetails")
    private JobDetails jobDetails;
    @JsonProperty("CustomerDetails")
    private CustomerDetails customerDetails;
    @JsonProperty("UnitDetails")
    private UnitDetails unitDetails;
    @JsonProperty("WarrantyDetails")
    private WarrantyDetails warrantyDetails;
    @JsonProperty("FaultReport")
    private FaultReport faultReport;
    @JsonProperty("ServiceHistory")
    private ServiceHistory serviceHistory;
    @JsonProperty("ContactHistories")
    private ContactHistories contactHistories;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("AdditionalEngineerID")
    public String getAdditionalEngineerID() {
        return additionalEngineerID;
    }

    @JsonProperty("AdditionalEngineerID")
    public void setAdditionalEngineerID(String additionalEngineerID) {
        this.additionalEngineerID = additionalEngineerID;
    }

    @JsonProperty("SLAppID")
    public String getSLAppID() {
        return sLAppID;
    }

    @JsonProperty("SLAppID")
    public void setSLAppID(String sLAppID) {
        this.sLAppID = sLAppID;
    }

    @JsonProperty("SortOrder")
    public String getSortOrder() {
        return sortOrder;
    }

    @JsonProperty("SortOrder")
    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    @JsonProperty("AppointmentType")
    public String getAppointmentType() {
        return appointmentType;
    }

    @JsonProperty("AppointmentType")
    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    @JsonProperty("ServiceProviderID")
    public String getServiceProviderID() {
        return serviceProviderID;
    }

    @JsonProperty("ServiceProviderID")
    public void setServiceProviderID(String serviceProviderID) {
        this.serviceProviderID = serviceProviderID;
    }

    @JsonProperty("SendCustomerReceiptEmail")
    public String getSendCustomerReceiptEmail() {
        return sendCustomerReceiptEmail;
    }

    @JsonProperty("SendCustomerReceiptEmail")
    public void setSendCustomerReceiptEmail(String sendCustomerReceiptEmail) {
        this.sendCustomerReceiptEmail = sendCustomerReceiptEmail;
    }

    @JsonProperty("TimeSlot")
    public String getTimeSlot() {
        return timeSlot;
    }

    @JsonProperty("TimeSlot")
    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    @JsonProperty("AppointmentDefaults")
    public AppointmentDefaults getAppointmentDefaults() {
        return appointmentDefaults;
    }

    @JsonProperty("AppointmentDefaults")
    public void setAppointmentDefaults(AppointmentDefaults appointmentDefaults) {
        this.appointmentDefaults = appointmentDefaults;
    }

    @JsonProperty("JobDetails")
    public JobDetails getJobDetails() {
        return jobDetails;
    }

    @JsonProperty("JobDetails")
    public void setJobDetails(JobDetails jobDetails) {
        this.jobDetails = jobDetails;
    }

    @JsonProperty("CustomerDetails")
    public CustomerDetails getCustomerDetails() {
        return customerDetails;
    }

    @JsonProperty("CustomerDetails")
    public void setCustomerDetails(CustomerDetails customerDetails) {
        this.customerDetails = customerDetails;
    }

    @JsonProperty("UnitDetails")
    public UnitDetails getUnitDetails() {
        return unitDetails;
    }

    @JsonProperty("UnitDetails")
    public void setUnitDetails(UnitDetails unitDetails) {
        this.unitDetails = unitDetails;
    }

    @JsonProperty("WarrantyDetails")
    public WarrantyDetails getWarrantyDetails() {
        return warrantyDetails;
    }

    @JsonProperty("WarrantyDetails")
    public void setWarrantyDetails(WarrantyDetails warrantyDetails) {
        this.warrantyDetails = warrantyDetails;
    }

    @JsonProperty("FaultReport")
    public FaultReport getFaultReport() {
        return faultReport;
    }

    @JsonProperty("FaultReport")
    public void setFaultReport(FaultReport faultReport) {
        this.faultReport = faultReport;
    }

    @JsonProperty("ServiceHistory")
    public ServiceHistory getServiceHistory() {
        return serviceHistory;
    }

    @JsonProperty("ServiceHistory")
    public void setServiceHistory(ServiceHistory serviceHistory) {
        this.serviceHistory = serviceHistory;
    }

    @JsonProperty("ContactHistories")
    public ContactHistories getContactHistories() {
        return contactHistories;
    }

    @JsonProperty("ContactHistories")
    public void setContactHistories(ContactHistories contactHistories) {
        this.contactHistories = contactHistories;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
