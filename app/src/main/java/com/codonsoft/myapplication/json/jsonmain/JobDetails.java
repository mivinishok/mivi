
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JobDetails {

    @SerializedName("AppointmentTime")
    @Expose
    private String appointmentTime;
    @SerializedName("JobNo")
    @Expose
    private String jobNo;
    @SerializedName("SLJobNo")
    @Expose
    private String sLJobNo;
    @SerializedName("ArgosSparesManagement")
    @Expose
    private String argosSparesManagement;
    @SerializedName("ClientNumber")
    @Expose
    private List<Object> clientNumber = null;
    @SerializedName("IncidentNumber")
    @Expose
    private List<Object> incidentNumber = null;
    @SerializedName("RepairType")
    @Expose
    private List<Object> repairType = null;
    @SerializedName("EstimateRequired")
    @Expose
    private String estimateRequired;
    @SerializedName("EstimateStatus")
    @Expose
    private List<Object> estimateStatus = null;
    @SerializedName("EstimateIfOver")
    @Expose
    private String estimateIfOver;
    @SerializedName("EstimateAccepted")
    @Expose
    private String estimateAccepted;
    @SerializedName("EstimateRejected")
    @Expose
    private String estimateRejected;
    @SerializedName("AppointmentNotes")
    @Expose
    private String appointmentNotes;
    @SerializedName("WarrantyJob")
    @Expose
    private String warrantyJob;
    @SerializedName("JobTypeID")
    @Expose
    private String jobTypeID;
    @SerializedName("PreviousVisits")
    @Expose
    private String previousVisits;
    @SerializedName("DescriptionOfRepair")
    @Expose
    private List<Object> descriptionOfRepair = null;
    @SerializedName("DisplayChargeableCosts")
    @Expose
    private String displayChargeableCosts;
    @SerializedName("ChargeableLabourCost")
    @Expose
    private String chargeableLabourCost;
    @SerializedName("ChargeablePartsCost")
    @Expose
    private String chargeablePartsCost;
    @SerializedName("CollectionDeliveryCost")
    @Expose
    private String collectionDeliveryCost;
    @SerializedName("ChargeableVATCost")
    @Expose
    private String chargeableVATCost;
    @SerializedName("ChargeableTotalCost")
    @Expose
    private String chargeableTotalCost;
    @SerializedName("ChargeableTotalPaid")
    @Expose
    private String chargeableTotalPaid;
    @SerializedName("ChargeableRefund")
    @Expose
    private String chargeableRefund;
    @SerializedName("ChargableBalanceDue")
    @Expose
    private String chargableBalanceDue;
    @SerializedName("JobCostID")
    @Expose
    private String jobCostID;
    @SerializedName("ChargeableLabourVATCost")
    @Expose
    private String chargeableLabourVATCost;
    @SerializedName("ChargeableSubTotal")
    @Expose
    private String chargeableSubTotal;
    @SerializedName("ChargeableInvoiceNumber")
    @Expose
    private List<Object> chargeableInvoiceNumber = null;
    @SerializedName("ChargeableInvoiceDate")
    @Expose
    private List<Object> chargeableInvoiceDate = null;
    @SerializedName("ChargeablePriceLock")
    @Expose
    private String chargeablePriceLock;
    @SerializedName("ChargeableNetworkManagerOverride")
    @Expose
    private String chargeableNetworkManagerOverride;
    @SerializedName("ChargeableComments")
    @Expose
    private String chargeableComments;
    @SerializedName("ChargeableApproved")
    @Expose
    private String chargeableApproved;
    @SerializedName("ChargeableModifiedBy")
    @Expose
    private String chargeableModifiedBy;
    @SerializedName("LoanApproved")
    @Expose
    private List<Object> loanApproved = null;
    @SerializedName("AppointmentWindow")
    @Expose
    private String appointmentWindow;
    @SerializedName("TravelTime")
    @Expose
    private String travelTime;
    @SerializedName("Duration")
    @Expose
    private String duration;
    @SerializedName("PreVisit")
    @Expose
    private List<Object> preVisit = null;
    @SerializedName("Network")
    @Expose
    private String network;
    @SerializedName("NetworkID")
    @Expose
    private String networkID;
    @SerializedName("Client")
    @Expose
    private String client;
    @SerializedName("ClientID")
    @Expose
    private String clientID;
    @SerializedName("Branch")
    @Expose
    private String branch;
    @SerializedName("BranchID")
    @Expose
    private String branchID;
    @SerializedName("BranchOpenTime")
    @Expose
    private String branchOpenTime;
    @SerializedName("BranchCloseTime")
    @Expose
    private String branchCloseTime;
    @SerializedName("RepeatRepair")
    @Expose
    private String repeatRepair;
    @SerializedName("ServiceTypeID1")
    @Expose
    private String serviceTypeID1;
    @SerializedName("ServiceTypeName1")
    @Expose
    private String serviceTypeName1;
    @SerializedName("DGJobFaultCode")
    @Expose
    private List<Object> dGJobFaultCode = null;
    @SerializedName("ServiceTypeIDs")
    @Expose
    private List<String> serviceTypeIDs = null;
    @SerializedName("UnitTypeID")
    @Expose
    private String unitTypeID;
    @SerializedName("RepairSkillID")
    @Expose
    private List<Object> repairSkillID = null;

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getJobNo() {
        return jobNo;
    }

    public void setJobNo(String jobNo) {
        this.jobNo = jobNo;
    }

    public String getSLJobNo() {
        return sLJobNo;
    }

    public void setSLJobNo(String sLJobNo) {
        this.sLJobNo = sLJobNo;
    }

    public String getArgosSparesManagement() {
        return argosSparesManagement;
    }

    public void setArgosSparesManagement(String argosSparesManagement) {
        this.argosSparesManagement = argosSparesManagement;
    }

    public List<Object> getClientNumber() {
        return clientNumber;
    }

    public void setClientNumber(List<Object> clientNumber) {
        this.clientNumber = clientNumber;
    }

    public List<Object> getIncidentNumber() {
        return incidentNumber;
    }

    public void setIncidentNumber(List<Object> incidentNumber) {
        this.incidentNumber = incidentNumber;
    }

    public List<Object> getRepairType() {
        return repairType;
    }

    public void setRepairType(List<Object> repairType) {
        this.repairType = repairType;
    }

    public String getEstimateRequired() {
        return estimateRequired;
    }

    public void setEstimateRequired(String estimateRequired) {
        this.estimateRequired = estimateRequired;
    }

    public List<Object> getEstimateStatus() {
        return estimateStatus;
    }

    public void setEstimateStatus(List<Object> estimateStatus) {
        this.estimateStatus = estimateStatus;
    }

    public String getEstimateIfOver() {
        return estimateIfOver;
    }

    public void setEstimateIfOver(String estimateIfOver) {
        this.estimateIfOver = estimateIfOver;
    }

    public String getEstimateAccepted() {
        return estimateAccepted;
    }

    public void setEstimateAccepted(String estimateAccepted) {
        this.estimateAccepted = estimateAccepted;
    }

    public String getEstimateRejected() {
        return estimateRejected;
    }

    public void setEstimateRejected(String estimateRejected) {
        this.estimateRejected = estimateRejected;
    }

    public String getAppointmentNotes() {
        return appointmentNotes;
    }

    public void setAppointmentNotes(String appointmentNotes) {
        this.appointmentNotes = appointmentNotes;
    }

    public String getWarrantyJob() {
        return warrantyJob;
    }

    public void setWarrantyJob(String warrantyJob) {
        this.warrantyJob = warrantyJob;
    }

    public String getJobTypeID() {
        return jobTypeID;
    }

    public void setJobTypeID(String jobTypeID) {
        this.jobTypeID = jobTypeID;
    }

    public String getPreviousVisits() {
        return previousVisits;
    }

    public void setPreviousVisits(String previousVisits) {
        this.previousVisits = previousVisits;
    }

    public List<Object> getDescriptionOfRepair() {
        return descriptionOfRepair;
    }

    public void setDescriptionOfRepair(List<Object> descriptionOfRepair) {
        this.descriptionOfRepair = descriptionOfRepair;
    }

    public String getDisplayChargeableCosts() {
        return displayChargeableCosts;
    }

    public void setDisplayChargeableCosts(String displayChargeableCosts) {
        this.displayChargeableCosts = displayChargeableCosts;
    }

    public String getChargeableLabourCost() {
        return chargeableLabourCost;
    }

    public void setChargeableLabourCost(String chargeableLabourCost) {
        this.chargeableLabourCost = chargeableLabourCost;
    }

    public String getChargeablePartsCost() {
        return chargeablePartsCost;
    }

    public void setChargeablePartsCost(String chargeablePartsCost) {
        this.chargeablePartsCost = chargeablePartsCost;
    }

    public String getCollectionDeliveryCost() {
        return collectionDeliveryCost;
    }

    public void setCollectionDeliveryCost(String collectionDeliveryCost) {
        this.collectionDeliveryCost = collectionDeliveryCost;
    }

    public String getChargeableVATCost() {
        return chargeableVATCost;
    }

    public void setChargeableVATCost(String chargeableVATCost) {
        this.chargeableVATCost = chargeableVATCost;
    }

    public String getChargeableTotalCost() {
        return chargeableTotalCost;
    }

    public void setChargeableTotalCost(String chargeableTotalCost) {
        this.chargeableTotalCost = chargeableTotalCost;
    }

    public String getChargeableTotalPaid() {
        return chargeableTotalPaid;
    }

    public void setChargeableTotalPaid(String chargeableTotalPaid) {
        this.chargeableTotalPaid = chargeableTotalPaid;
    }

    public String getChargeableRefund() {
        return chargeableRefund;
    }

    public void setChargeableRefund(String chargeableRefund) {
        this.chargeableRefund = chargeableRefund;
    }

    public String getChargableBalanceDue() {
        return chargableBalanceDue;
    }

    public void setChargableBalanceDue(String chargableBalanceDue) {
        this.chargableBalanceDue = chargableBalanceDue;
    }

    public String getJobCostID() {
        return jobCostID;
    }

    public void setJobCostID(String jobCostID) {
        this.jobCostID = jobCostID;
    }

    public String getChargeableLabourVATCost() {
        return chargeableLabourVATCost;
    }

    public void setChargeableLabourVATCost(String chargeableLabourVATCost) {
        this.chargeableLabourVATCost = chargeableLabourVATCost;
    }

    public String getChargeableSubTotal() {
        return chargeableSubTotal;
    }

    public void setChargeableSubTotal(String chargeableSubTotal) {
        this.chargeableSubTotal = chargeableSubTotal;
    }

    public List<Object> getChargeableInvoiceNumber() {
        return chargeableInvoiceNumber;
    }

    public void setChargeableInvoiceNumber(List<Object> chargeableInvoiceNumber) {
        this.chargeableInvoiceNumber = chargeableInvoiceNumber;
    }

    public List<Object> getChargeableInvoiceDate() {
        return chargeableInvoiceDate;
    }

    public void setChargeableInvoiceDate(List<Object> chargeableInvoiceDate) {
        this.chargeableInvoiceDate = chargeableInvoiceDate;
    }

    public String getChargeablePriceLock() {
        return chargeablePriceLock;
    }

    public void setChargeablePriceLock(String chargeablePriceLock) {
        this.chargeablePriceLock = chargeablePriceLock;
    }

    public String getChargeableNetworkManagerOverride() {
        return chargeableNetworkManagerOverride;
    }

    public void setChargeableNetworkManagerOverride(String chargeableNetworkManagerOverride) {
        this.chargeableNetworkManagerOverride = chargeableNetworkManagerOverride;
    }

    public String getChargeableComments() {
        return chargeableComments;
    }

    public void setChargeableComments(String chargeableComments) {
        this.chargeableComments = chargeableComments;
    }

    public String getChargeableApproved() {
        return chargeableApproved;
    }

    public void setChargeableApproved(String chargeableApproved) {
        this.chargeableApproved = chargeableApproved;
    }

    public String getChargeableModifiedBy() {
        return chargeableModifiedBy;
    }

    public void setChargeableModifiedBy(String chargeableModifiedBy) {
        this.chargeableModifiedBy = chargeableModifiedBy;
    }

    public List<Object> getLoanApproved() {
        return loanApproved;
    }

    public void setLoanApproved(List<Object> loanApproved) {
        this.loanApproved = loanApproved;
    }

    public String getAppointmentWindow() {
        return appointmentWindow;
    }

    public void setAppointmentWindow(String appointmentWindow) {
        this.appointmentWindow = appointmentWindow;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public List<Object> getPreVisit() {
        return preVisit;
    }

    public void setPreVisit(List<Object> preVisit) {
        this.preVisit = preVisit;
    }

    public String getNetwork() {
        return network;
    }

    public void setNetwork(String network) {
        this.network = network;
    }

    public String getNetworkID() {
        return networkID;
    }

    public void setNetworkID(String networkID) {
        this.networkID = networkID;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchID() {
        return branchID;
    }

    public void setBranchID(String branchID) {
        this.branchID = branchID;
    }

    public String getBranchOpenTime() {
        return branchOpenTime;
    }

    public void setBranchOpenTime(String branchOpenTime) {
        this.branchOpenTime = branchOpenTime;
    }

    public String getBranchCloseTime() {
        return branchCloseTime;
    }

    public void setBranchCloseTime(String branchCloseTime) {
        this.branchCloseTime = branchCloseTime;
    }

    public String getRepeatRepair() {
        return repeatRepair;
    }

    public void setRepeatRepair(String repeatRepair) {
        this.repeatRepair = repeatRepair;
    }

    public String getServiceTypeID1() {
        return serviceTypeID1;
    }

    public void setServiceTypeID1(String serviceTypeID1) {
        this.serviceTypeID1 = serviceTypeID1;
    }

    public String getServiceTypeName1() {
        return serviceTypeName1;
    }

    public void setServiceTypeName1(String serviceTypeName1) {
        this.serviceTypeName1 = serviceTypeName1;
    }

    public List<Object> getDGJobFaultCode() {
        return dGJobFaultCode;
    }

    public void setDGJobFaultCode(List<Object> dGJobFaultCode) {
        this.dGJobFaultCode = dGJobFaultCode;
    }

    public List<String> getServiceTypeIDs() {
        return serviceTypeIDs;
    }

    public void setServiceTypeIDs(List<String> serviceTypeIDs) {
        this.serviceTypeIDs = serviceTypeIDs;
    }

    public String getUnitTypeID() {
        return unitTypeID;
    }

    public void setUnitTypeID(String unitTypeID) {
        this.unitTypeID = unitTypeID;
    }

    public List<Object> getRepairSkillID() {
        return repairSkillID;
    }

    public void setRepairSkillID(List<Object> repairSkillID) {
        this.repairSkillID = repairSkillID;
    }

}
