
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UnitDetails {

    @SerializedName("Make")
    @Expose
    private String make;
    @SerializedName("ManufacturerID")
    @Expose
    private String manufacturerID;
    @SerializedName("Model")
    @Expose
    private String model;
    @SerializedName("ModelID")
    @Expose
    private String modelID;
    @SerializedName("UnitType")
    @Expose
    private String unitType;
    @SerializedName("GCNumber")
    @Expose
    private List<Object> gCNumber = null;
    @SerializedName("ProductNo")
    @Expose
    private List<Object> productNo = null;
    @SerializedName("SerialNo")
    @Expose
    private List<Object> serialNo = null;
    @SerializedName("DOP")
    @Expose
    private List<Object> dOP = null;
    @SerializedName("OldFirmwareVersion")
    @Expose
    private List<Object> oldFirmwareVersion = null;
    @SerializedName("NewFirmwareVersion")
    @Expose
    private List<Object> newFirmwareVersion = null;
    @SerializedName("CatalogueNo")
    @Expose
    private List<Object> catalogueNo = null;
    @SerializedName("UnitCondition")
    @Expose
    private List<Object> unitCondition = null;
    @SerializedName("UnitLocation")
    @Expose
    private List<Object> unitLocation = null;
    @SerializedName("Accessories")
    @Expose
    private List<Object> accessories = null;
    @SerializedName("LoanUnitNumber")
    @Expose
    private List<Object> loanUnitNumber = null;
    @SerializedName("LoanUnitManufacturer")
    @Expose
    private List<Object> loanUnitManufacturer = null;
    @SerializedName("LoanUnitModelNumber")
    @Expose
    private List<Object> loanUnitModelNumber = null;
    @SerializedName("LoanAccessoryList")
    @Expose
    private List<Object> loanAccessoryList = null;
    @SerializedName("JobAccessoryList")
    @Expose
    private List<Object> jobAccessoryList = null;
    @SerializedName("ProductPremisesLocation")
    @Expose
    private List<Object> productPremisesLocation = null;

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getManufacturerID() {
        return manufacturerID;
    }

    public void setManufacturerID(String manufacturerID) {
        this.manufacturerID = manufacturerID;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModelID() {
        return modelID;
    }

    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public List<Object> getGCNumber() {
        return gCNumber;
    }

    public void setGCNumber(List<Object> gCNumber) {
        this.gCNumber = gCNumber;
    }

    public List<Object> getProductNo() {
        return productNo;
    }

    public void setProductNo(List<Object> productNo) {
        this.productNo = productNo;
    }

    public List<Object> getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(List<Object> serialNo) {
        this.serialNo = serialNo;
    }

    public List<Object> getDOP() {
        return dOP;
    }

    public void setDOP(List<Object> dOP) {
        this.dOP = dOP;
    }

    public List<Object> getOldFirmwareVersion() {
        return oldFirmwareVersion;
    }

    public void setOldFirmwareVersion(List<Object> oldFirmwareVersion) {
        this.oldFirmwareVersion = oldFirmwareVersion;
    }

    public List<Object> getNewFirmwareVersion() {
        return newFirmwareVersion;
    }

    public void setNewFirmwareVersion(List<Object> newFirmwareVersion) {
        this.newFirmwareVersion = newFirmwareVersion;
    }

    public List<Object> getCatalogueNo() {
        return catalogueNo;
    }

    public void setCatalogueNo(List<Object> catalogueNo) {
        this.catalogueNo = catalogueNo;
    }

    public List<Object> getUnitCondition() {
        return unitCondition;
    }

    public void setUnitCondition(List<Object> unitCondition) {
        this.unitCondition = unitCondition;
    }

    public List<Object> getUnitLocation() {
        return unitLocation;
    }

    public void setUnitLocation(List<Object> unitLocation) {
        this.unitLocation = unitLocation;
    }

    public List<Object> getAccessories() {
        return accessories;
    }

    public void setAccessories(List<Object> accessories) {
        this.accessories = accessories;
    }

    public List<Object> getLoanUnitNumber() {
        return loanUnitNumber;
    }

    public void setLoanUnitNumber(List<Object> loanUnitNumber) {
        this.loanUnitNumber = loanUnitNumber;
    }

    public List<Object> getLoanUnitManufacturer() {
        return loanUnitManufacturer;
    }

    public void setLoanUnitManufacturer(List<Object> loanUnitManufacturer) {
        this.loanUnitManufacturer = loanUnitManufacturer;
    }

    public List<Object> getLoanUnitModelNumber() {
        return loanUnitModelNumber;
    }

    public void setLoanUnitModelNumber(List<Object> loanUnitModelNumber) {
        this.loanUnitModelNumber = loanUnitModelNumber;
    }

    public List<Object> getLoanAccessoryList() {
        return loanAccessoryList;
    }

    public void setLoanAccessoryList(List<Object> loanAccessoryList) {
        this.loanAccessoryList = loanAccessoryList;
    }

    public List<Object> getJobAccessoryList() {
        return jobAccessoryList;
    }

    public void setJobAccessoryList(List<Object> jobAccessoryList) {
        this.jobAccessoryList = jobAccessoryList;
    }

    public List<Object> getProductPremisesLocation() {
        return productPremisesLocation;
    }

    public void setProductPremisesLocation(List<Object> productPremisesLocation) {
        this.productPremisesLocation = productPremisesLocation;
    }

}
