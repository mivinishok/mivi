
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "DataConsent",
    "CustomerTitle",
    "CustomerForename",
    "CustomerSurname",
    "CustomerBuildingName",
    "CustomerStreet",
    "CustomerAddressTown",
    "CustomerCounty",
    "CustomerPostcode",
    "CustomerMobileNo",
    "UseCollectionAddress",
    "CollectionBuildingName",
    "CollectionStreet",
    "CollectionAddressTown",
    "CollectionCounty",
    "CollectionPostCode",
    "ApptLatitude",
        "CustomerCompanyName",
        "CollectionCompanyName",
        "CustomerAddressArea",
        "CollectionAddressArea",
    "ApptLongitude"
})
public class CustomerDetails {

    @JsonProperty("CustomerAddressArea")
    private String customerAddressArea = null;
    @JsonProperty("CollectionAddressArea")
    private String collectionAddressArea = null;
    @JsonProperty("CollectionCompanyName")
    private String collectionCompanyName = null;
    @JsonProperty("CustomerCompanyName")
    private String customerCompanyName = null;
    @JsonProperty("DataConsent")
    private String dataConsent;
    @JsonProperty("CustomerTitle")
    private String customerTitle;
    @JsonProperty("CustomerForename")
    private String customerForename;
    @JsonProperty("CustomerSurname")
    private String customerSurname;
    @JsonProperty("CustomerBuildingName")
    private String customerBuildingName;
    @JsonProperty("CustomerStreet")
    private String customerStreet;
    @JsonProperty("CustomerAddressTown")
    private String customerAddressTown;
    @JsonProperty("CustomerCounty")
    private String customerCounty;
    @JsonProperty("CustomerPostcode")
    private String customerPostcode;
    @JsonProperty("CustomerMobileNo")
    private String customerMobileNo;
    @JsonProperty("UseCollectionAddress")
    private String useCollectionAddress;
    @JsonProperty("CollectionBuildingName")
    private String collectionBuildingName;
    @JsonProperty("CollectionStreet")
    private String collectionStreet;
    @JsonProperty("CollectionAddressTown")
    private String collectionAddressTown;
    @JsonProperty("CollectionCounty")
    private String collectionCounty;
    @JsonProperty("CollectionPostCode")
    private String collectionPostCode;
    @JsonProperty("ApptLatitude")
    private String apptLatitude;
    @JsonProperty("ApptLongitude")
    private String apptLongitude;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("CustomerAddressArea")
    public String getCustomerAddressArea() {
        return customerAddressArea;
    }

    @JsonProperty("CustomerAddressArea")
    public void setCustomerAddressArea(String customerAddressArea) {
        this.customerAddressArea = customerAddressArea;
    }

    @JsonProperty("CollectionAddressArea")
    public String getCollectionAddressArea() {
        return collectionAddressArea;
    }

    @JsonProperty("CollectionAddressArea")
    public void setCollectionAddressArea(String collectionAddressArea) {
        this.collectionAddressArea = collectionAddressArea;
    }
    @JsonProperty("CustomerCompanyName")
    public String getCustomerCompanyName() {
        return customerCompanyName;
    }

    @JsonProperty("CustomerCompanyName")
    public void setCustomerCompanyName(String customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    @JsonProperty("CollectionCompanyName")
    public String getCollectionCompanyName() {
        return collectionCompanyName;
    }

    @JsonProperty("CollectionCompanyName")
    public void setCollectionCompanyName(String collectionCompanyName) {
        this.collectionCompanyName = collectionCompanyName;
    }


    @JsonProperty("DataConsent")
    public String getDataConsent() {
        return dataConsent;
    }

    @JsonProperty("DataConsent")
    public void setDataConsent(String dataConsent) {
        this.dataConsent = dataConsent;
    }

    @JsonProperty("CustomerTitle")
    public String getCustomerTitle() {
        return customerTitle;
    }

    @JsonProperty("CustomerTitle")
    public void setCustomerTitle(String customerTitle) {
        this.customerTitle = customerTitle;
    }

    @JsonProperty("CustomerForename")
    public String getCustomerForename() {
        return customerForename;
    }

    @JsonProperty("CustomerForename")
    public void setCustomerForename(String customerForename) {
        this.customerForename = customerForename;
    }

    @JsonProperty("CustomerSurname")
    public String getCustomerSurname() {
        return customerSurname;
    }

    @JsonProperty("CustomerSurname")
    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    @JsonProperty("CustomerBuildingName")
    public String getCustomerBuildingName() {
        return customerBuildingName;
    }

    @JsonProperty("CustomerBuildingName")
    public void setCustomerBuildingName(String customerBuildingName) {
        this.customerBuildingName = customerBuildingName;
    }

    @JsonProperty("CustomerStreet")
    public String getCustomerStreet() {
        return customerStreet;
    }

    @JsonProperty("CustomerStreet")
    public void setCustomerStreet(String customerStreet) {
        this.customerStreet = customerStreet;
    }

    @JsonProperty("CustomerAddressTown")
    public String getCustomerAddressTown() {
        return customerAddressTown;
    }

    @JsonProperty("CustomerAddressTown")
    public void setCustomerAddressTown(String customerAddressTown) {
        this.customerAddressTown = customerAddressTown;
    }

    @JsonProperty("CustomerCounty")
    public String getCustomerCounty() {
        return customerCounty;
    }

    @JsonProperty("CustomerCounty")
    public void setCustomerCounty(String customerCounty) {
        this.customerCounty = customerCounty;
    }

    @JsonProperty("CustomerPostcode")
    public String getCustomerPostcode() {
        return customerPostcode;
    }

    @JsonProperty("CustomerPostcode")
    public void setCustomerPostcode(String customerPostcode) {
        this.customerPostcode = customerPostcode;
    }

    @JsonProperty("CustomerMobileNo")
    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    @JsonProperty("CustomerMobileNo")
    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    @JsonProperty("UseCollectionAddress")
    public String getUseCollectionAddress() {
        return useCollectionAddress;
    }

    @JsonProperty("UseCollectionAddress")
    public void setUseCollectionAddress(String useCollectionAddress) {
        this.useCollectionAddress = useCollectionAddress;
    }

    @JsonProperty("CollectionBuildingName")
    public String getCollectionBuildingName() {
        return collectionBuildingName;
    }

    @JsonProperty("CollectionBuildingName")
    public void setCollectionBuildingName(String collectionBuildingName) {
        this.collectionBuildingName = collectionBuildingName;
    }

    @JsonProperty("CollectionStreet")
    public String getCollectionStreet() {
        return collectionStreet;
    }

    @JsonProperty("CollectionStreet")
    public void setCollectionStreet(String collectionStreet) {
        this.collectionStreet = collectionStreet;
    }

    @JsonProperty("CollectionAddressTown")
    public String getCollectionAddressTown() {
        return collectionAddressTown;
    }

    @JsonProperty("CollectionAddressTown")
    public void setCollectionAddressTown(String collectionAddressTown) {
        this.collectionAddressTown = collectionAddressTown;
    }

    @JsonProperty("CollectionCounty")
    public String getCollectionCounty() {
        return collectionCounty;
    }

    @JsonProperty("CollectionCounty")
    public void setCollectionCounty(String collectionCounty) {
        this.collectionCounty = collectionCounty;
    }

    @JsonProperty("CollectionPostCode")
    public String getCollectionPostCode() {
        return collectionPostCode;
    }

    @JsonProperty("CollectionPostCode")
    public void setCollectionPostCode(String collectionPostCode) {
        this.collectionPostCode = collectionPostCode;
    }

    @JsonProperty("ApptLatitude")
    public String getApptLatitude() {
        return apptLatitude;
    }

    @JsonProperty("ApptLatitude")
    public void setApptLatitude(String apptLatitude) {
        this.apptLatitude = apptLatitude;
    }

    @JsonProperty("ApptLongitude")
    public String getApptLongitude() {
        return apptLongitude;
    }

    @JsonProperty("ApptLongitude")
    public void setApptLongitude(String apptLongitude) {
        this.apptLongitude = apptLongitude;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
