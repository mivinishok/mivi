
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WarrantyDetails {

    @SerializedName("ChargeType")
    @Expose
    private String chargeType;
    @SerializedName("JobType")
    @Expose
    private List<Object> jobType = null;
    @SerializedName("OriginalRetailer")
    @Expose
    private List<Object> originalRetailer = null;
    @SerializedName("Insurer")
    @Expose
    private List<Object> insurer = null;
    @SerializedName("PolicyNo")
    @Expose
    private List<Object> policyNo = null;
    @SerializedName("AuthorisationNumber")
    @Expose
    private List<Object> authorisationNumber = null;
    @SerializedName("PolicyEndDate")
    @Expose
    private String policyEndDate;
    @SerializedName("FraudAlert")
    @Expose
    private List<Object> fraudAlert = null;
    @SerializedName("WarrantyNotes")
    @Expose
    private List<Object> warrantyNotes = null;

    public String getChargeType() {
        return chargeType;
    }

    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    public List<Object> getJobType() {
        return jobType;
    }

    public void setJobType(List<Object> jobType) {
        this.jobType = jobType;
    }

    public List<Object> getOriginalRetailer() {
        return originalRetailer;
    }

    public void setOriginalRetailer(List<Object> originalRetailer) {
        this.originalRetailer = originalRetailer;
    }

    public List<Object> getInsurer() {
        return insurer;
    }

    public void setInsurer(List<Object> insurer) {
        this.insurer = insurer;
    }

    public List<Object> getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(List<Object> policyNo) {
        this.policyNo = policyNo;
    }

    public List<Object> getAuthorisationNumber() {
        return authorisationNumber;
    }

    public void setAuthorisationNumber(List<Object> authorisationNumber) {
        this.authorisationNumber = authorisationNumber;
    }

    public String getPolicyEndDate() {
        return policyEndDate;
    }

    public void setPolicyEndDate(String policyEndDate) {
        this.policyEndDate = policyEndDate;
    }

    public List<Object> getFraudAlert() {
        return fraudAlert;
    }

    public void setFraudAlert(List<Object> fraudAlert) {
        this.fraudAlert = fraudAlert;
    }

    public List<Object> getWarrantyNotes() {
        return warrantyNotes;
    }

    public void setWarrantyNotes(List<Object> warrantyNotes) {
        this.warrantyNotes = warrantyNotes;
    }

}
