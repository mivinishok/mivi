
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Make",
    "ManufacturerID",
    "UnitType",
    "UnitCondition",
    "Model",
    "ModelID"
})
public class UnitDetails {

    @JsonProperty("Make")
    private String make;
    @JsonProperty("ManufacturerID")
    private String manufacturerID;
    @JsonProperty("UnitType")
    private String unitType;
    @JsonProperty("UnitCondition")
    private String unitCondition;
    @JsonProperty("Model")
    private String model;
    @JsonProperty("ModelID")
    private String modelID;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("Make")
    public String getMake() {
        return make;
    }

    @JsonProperty("Make")
    public void setMake(String make) {
        this.make = make;
    }

    @JsonProperty("ManufacturerID")
    public String getManufacturerID() {
        return manufacturerID;
    }

    @JsonProperty("ManufacturerID")
    public void setManufacturerID(String manufacturerID) {
        this.manufacturerID = manufacturerID;
    }

    @JsonProperty("UnitType")
    public String getUnitType() {
        return unitType;
    }

    @JsonProperty("UnitType")
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    @JsonProperty("UnitCondition")
    public String getUnitCondition() {
        return unitCondition;
    }

    @JsonProperty("UnitCondition")
    public void setUnitCondition(String unitCondition) {
        this.unitCondition = unitCondition;
    }

    @JsonProperty("Model")
    public String getModel() {
        return model;
    }

    @JsonProperty("Model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("ModelID")
    public String getModelID() {
        return modelID;
    }

    @JsonProperty("ModelID")
    public void setModelID(String modelID) {
        this.modelID = modelID;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
