
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComplaintsCustMain {

    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;
    @SerializedName("APIName")
    @Expose
    private String aPIName;
    @SerializedName("ResponseDescription")
    @Expose
    private String responseDescription;
    @SerializedName("Defaults")
    @Expose
    private Defaults defaults;
    @SerializedName("Appointments")
    @Expose
    private List<Appointment> appointments = null;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getAPIName() {
        return aPIName;
    }

    public void setAPIName(String aPIName) {
        this.aPIName = aPIName;
    }

    public String getResponseDescription() {
        return responseDescription;
    }

    public void setResponseDescription(String responseDescription) {
        this.responseDescription = responseDescription;
    }

    public Defaults getDefaults() {
        return defaults;
    }

    public void setDefaults(Defaults defaults) {
        this.defaults = defaults;
    }

    public List<Appointment> getAppointments() {
        return appointments;
    }

    public void setAppointments(List<Appointment> appointments) {
        this.appointments = appointments;
    }

}
