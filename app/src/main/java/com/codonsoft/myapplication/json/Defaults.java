
package com.codonsoft.myapplication.json;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "FullName",
    "StartTime",
    "FinishTime",
    "SLUsername",
    "SLPassword"
})
public class Defaults {

    @JsonProperty("FullName")
    private String fullName;
    @JsonProperty("StartTime")
    private String startTime;
    @JsonProperty("FinishTime")
    private String finishTime;
    @JsonProperty("SLUsername")
    private String sLUsername;
    @JsonProperty("SLPassword")
    private String sLPassword;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("FullName")
    public String getFullName() {
        return fullName;
    }

    @JsonProperty("FullName")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @JsonProperty("StartTime")
    public String getStartTime() {
        return startTime;
    }

    @JsonProperty("StartTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    @JsonProperty("FinishTime")
    public String getFinishTime() {
        return finishTime;
    }

    @JsonProperty("FinishTime")
    public void setFinishTime(String finishTime) {
        this.finishTime = finishTime;
    }

    @JsonProperty("SLUsername")
    public String getSLUsername() {
        return sLUsername;
    }

    @JsonProperty("SLUsername")
    public void setSLUsername(String sLUsername) {
        this.sLUsername = sLUsername;
    }

    @JsonProperty("SLPassword")
    public String getSLPassword() {
        return sLPassword;
    }

    @JsonProperty("SLPassword")
    public void setSLPassword(String sLPassword) {
        this.sLPassword = sLPassword;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
