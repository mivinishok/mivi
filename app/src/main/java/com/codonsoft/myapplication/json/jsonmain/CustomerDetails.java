
package com.codonsoft.myapplication.json.jsonmain;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerDetails {

    @SerializedName("DataConsent")
    @Expose
    private String dataConsent;
    @SerializedName("CustomerTitle")
    @Expose
    private String customerTitle;
    @SerializedName("CustomerForename")
    @Expose
    private String customerForename;
    @SerializedName("CustomerSurname")
    @Expose
    private String customerSurname;
    @SerializedName("CustomerCompanyName")
    @Expose
    private List<Object> customerCompanyName = null;
    @SerializedName("CustomerBuildingName")
    @Expose
    private String customerBuildingName;
    @SerializedName("CustomerStreet")
    @Expose
    private String customerStreet;
    @SerializedName("CustomerAddressArea")
    @Expose
    private List<Object> customerAddressArea = null;
    @SerializedName("CustomerAddressTown")
    @Expose
    private String customerAddressTown;
    @SerializedName("CustomerCounty")
    @Expose
    private List<Object> customerCounty = null;
    @SerializedName("CustomerPostcode")
    @Expose
    private String customerPostcode;
    @SerializedName("CustomerHomeTelNo")
    @Expose
    private List<Object> customerHomeTelNo = null;
    @SerializedName("CustomerWorkTelNo")
    @Expose
    private List<Object> customerWorkTelNo = null;
    @SerializedName("CustomerMobileNo")
    @Expose
    private String customerMobileNo;
    @SerializedName("CustomerEmailAddress")
    @Expose
    private List<Object> customerEmailAddress = null;
    @SerializedName("GeneralInstructions")
    @Expose
    private List<Object> generalInstructions = null;
    @SerializedName("JobNotes")
    @Expose
    private List<Object> jobNotes = null;
    @SerializedName("UseCollectionAddress")
    @Expose
    private String useCollectionAddress;
    @SerializedName("CollectionBuildingName")
    @Expose
    private String collectionBuildingName;
    @SerializedName("CollectionCompanyName")
    @Expose
    private List<Object> collectionCompanyName = null;
    @SerializedName("CollectionStreet")
    @Expose
    private String collectionStreet;
    @SerializedName("CollectionAddressArea")
    @Expose
    private List<Object> collectionAddressArea = null;
    @SerializedName("CollectionAddressTown")
    @Expose
    private String collectionAddressTown;
    @SerializedName("CollectionCounty")
    @Expose
    private List<Object> collectionCounty = null;
    @SerializedName("CollectionPostCode")
    @Expose
    private String collectionPostCode;
    @SerializedName("CollectionContactNumber")
    @Expose
    private List<Object> collectionContactNumber = null;
    @SerializedName("ApptLatitude")
    @Expose
    private String apptLatitude;
    @SerializedName("ApptLongitude")
    @Expose
    private String apptLongitude;

    public String getDataConsent() {
        return dataConsent;
    }

    public void setDataConsent(String dataConsent) {
        this.dataConsent = dataConsent;
    }

    public String getCustomerTitle() {
        return customerTitle;
    }

    public void setCustomerTitle(String customerTitle) {
        this.customerTitle = customerTitle;
    }

    public String getCustomerForename() {
        return customerForename;
    }

    public void setCustomerForename(String customerForename) {
        this.customerForename = customerForename;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public List<Object> getCustomerCompanyName() {
        return customerCompanyName;
    }

    public void setCustomerCompanyName(List<Object> customerCompanyName) {
        this.customerCompanyName = customerCompanyName;
    }

    public String getCustomerBuildingName() {
        return customerBuildingName;
    }

    public void setCustomerBuildingName(String customerBuildingName) {
        this.customerBuildingName = customerBuildingName;
    }

    public String getCustomerStreet() {
        return customerStreet;
    }

    public void setCustomerStreet(String customerStreet) {
        this.customerStreet = customerStreet;
    }

    public List<Object> getCustomerAddressArea() {
        return customerAddressArea;
    }

    public void setCustomerAddressArea(List<Object> customerAddressArea) {
        this.customerAddressArea = customerAddressArea;
    }

    public String getCustomerAddressTown() {
        return customerAddressTown;
    }

    public void setCustomerAddressTown(String customerAddressTown) {
        this.customerAddressTown = customerAddressTown;
    }

    public List<Object> getCustomerCounty() {
        return customerCounty;
    }

    public void setCustomerCounty(List<Object> customerCounty) {
        this.customerCounty = customerCounty;
    }

    public String getCustomerPostcode() {
        return customerPostcode;
    }

    public void setCustomerPostcode(String customerPostcode) {
        this.customerPostcode = customerPostcode;
    }

    public List<Object> getCustomerHomeTelNo() {
        return customerHomeTelNo;
    }

    public void setCustomerHomeTelNo(List<Object> customerHomeTelNo) {
        this.customerHomeTelNo = customerHomeTelNo;
    }

    public List<Object> getCustomerWorkTelNo() {
        return customerWorkTelNo;
    }

    public void setCustomerWorkTelNo(List<Object> customerWorkTelNo) {
        this.customerWorkTelNo = customerWorkTelNo;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public List<Object> getCustomerEmailAddress() {
        return customerEmailAddress;
    }

    public void setCustomerEmailAddress(List<Object> customerEmailAddress) {
        this.customerEmailAddress = customerEmailAddress;
    }

    public List<Object> getGeneralInstructions() {
        return generalInstructions;
    }

    public void setGeneralInstructions(List<Object> generalInstructions) {
        this.generalInstructions = generalInstructions;
    }

    public List<Object> getJobNotes() {
        return jobNotes;
    }

    public void setJobNotes(List<Object> jobNotes) {
        this.jobNotes = jobNotes;
    }

    public String getUseCollectionAddress() {
        return useCollectionAddress;
    }

    public void setUseCollectionAddress(String useCollectionAddress) {
        this.useCollectionAddress = useCollectionAddress;
    }

    public String getCollectionBuildingName() {
        return collectionBuildingName;
    }

    public void setCollectionBuildingName(String collectionBuildingName) {
        this.collectionBuildingName = collectionBuildingName;
    }

    public List<Object> getCollectionCompanyName() {
        return collectionCompanyName;
    }

    public void setCollectionCompanyName(List<Object> collectionCompanyName) {
        this.collectionCompanyName = collectionCompanyName;
    }

    public String getCollectionStreet() {
        return collectionStreet;
    }

    public void setCollectionStreet(String collectionStreet) {
        this.collectionStreet = collectionStreet;
    }

    public List<Object> getCollectionAddressArea() {
        return collectionAddressArea;
    }

    public void setCollectionAddressArea(List<Object> collectionAddressArea) {
        this.collectionAddressArea = collectionAddressArea;
    }

    public String getCollectionAddressTown() {
        return collectionAddressTown;
    }

    public void setCollectionAddressTown(String collectionAddressTown) {
        this.collectionAddressTown = collectionAddressTown;
    }

    public List<Object> getCollectionCounty() {
        return collectionCounty;
    }

    public void setCollectionCounty(List<Object> collectionCounty) {
        this.collectionCounty = collectionCounty;
    }

    public String getCollectionPostCode() {
        return collectionPostCode;
    }

    public void setCollectionPostCode(String collectionPostCode) {
        this.collectionPostCode = collectionPostCode;
    }

    public List<Object> getCollectionContactNumber() {
        return collectionContactNumber;
    }

    public void setCollectionContactNumber(List<Object> collectionContactNumber) {
        this.collectionContactNumber = collectionContactNumber;
    }

    public String getApptLatitude() {
        return apptLatitude;
    }

    public void setApptLatitude(String apptLatitude) {
        this.apptLatitude = apptLatitude;
    }

    public String getApptLongitude() {
        return apptLongitude;
    }

    public void setApptLongitude(String apptLongitude) {
        this.apptLongitude = apptLongitude;
    }

}
