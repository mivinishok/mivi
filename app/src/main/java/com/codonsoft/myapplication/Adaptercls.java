package com.codonsoft.myapplication;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static com.codonsoft.myapplication.MainActivity.length;

/**
 * Created by nsar on 2/11/2018.
 */

public class Adaptercls extends BaseAdapter {

    String [][] result;
    Context context;


    private static LayoutInflater inflater=null;
    public Adaptercls(Main2Activity mainActivity, String[][] prgmNameList) {
        // TODO Auto-generated constructor stub
        result=prgmNameList;
        context=mainActivity;

        inflater = ( LayoutInflater )context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return result.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView tv ,tv2,tv3;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        Holder holder=new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.cust, null);
        holder.tv=(TextView) rowView.findViewById(R.id.textView9);
        holder.tv2=(TextView) rowView.findViewById(R.id.textView10);
        holder.tv3=(TextView) rowView.findViewById(R.id.textView10);

        holder.tv.setText(result[position][0] + " " +result[position][1]+" "+result[position][2]);

        holder.tv2.setText(result[position][3]+" "+result[position][4]+" \n"+result[position][5]+" "+result[position][6]+" "+result[position][7]+" \n"+result[position][8]);
//
        holder.tv3.setText(result[position][9] + " " +result[position][10]);
        final ImageButton button = rowView.findViewById(R.id.imageButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String dial = "tel:" + result[position][12];


                context.startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse(dial)));

                // Code here executes on main thread after user presses button
            }
        });

        final ImageButton button1 = rowView.findViewById(R.id.imageButton2);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent myIntent = new Intent(context, MapsActivity.class);
                myIntent.putExtra("EXTRA_SESSION_ID", result[position][11]);
                context.startActivity(myIntent);

                // Code here executes on main thread after user presses button
            }
        });
        return rowView;
    }

}
