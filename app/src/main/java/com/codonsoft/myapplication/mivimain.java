package com.codonsoft.myapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.codonsoft.myapplication.json.ComplaintsCust;
import com.codonsoft.myapplication.json.mivi.Mivi;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class mivimain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mivimain);

        TextView name = (TextView) findViewById(R.id.name);
        TextView dateplan = (TextView) findViewById(R.id.pack);
        TextView balance = (TextView) findViewById(R.id.balance);

        String name_text = getIntent().getStringExtra("name");
        String dateplan_text = getIntent().getStringExtra("plan");
        String balance_text = getIntent().getStringExtra("balance");

        setTitle(name_text);
        dateplan.setText(dateplan_text);
        balance.setText(balance_text);



    }

    boolean doubleBackToExitPressedOnce = false;
    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            Intent myIntent = new Intent(mivimain.this, LoginActivity.class);
            mivimain.this.startActivity(myIntent);
            finish();
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to Logout", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
}
