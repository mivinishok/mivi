package com.codonsoft.myapplication;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.codonsoft.myapplication.json.mivi.Mivi;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

public class miviflash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miviflash);

        final Intent myIntent = new Intent(miviflash.this, mivimain.class);


        JSONObject jsonObject= null;
        ObjectMapper mapper = new ObjectMapper();
        try {
            jsonObject = new JSONObject(getJson());
            String s  = jsonObject.toString();
            Mivi emp = null;
            try {
                emp = mapper.readValue(jsonObject.toString(), Mivi.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

            myIntent.putExtra("name","Hi "+emp.getData().getAttributes().getTitle().toString()+". " + emp.getData().getAttributes().getFirstName().toString()+" "+emp.getData().getAttributes().getLastName().toString());
            for (int i=0;i<emp.getIncluded().size();i++)
            {
                if(emp.getIncluded().get(i).getType().equals("subscriptions"))
                {

                    myIntent.putExtra("plan",emp.getIncluded().get(1).getAttributes().getIncludedDataBalance().toString()+" Mb \n"+emp.getIncluded().get(1).getAttributes().getExpiryDate().toString());
                }
                else if(emp.getIncluded().get(i).getType().equals("products"))
                {

                    myIntent.putExtra("balance",emp.getIncluded().get(2).getAttributes().getName().toString()+"\n"+emp.getIncluded().get(2).getAttributes().getPrice().toString());
                }
            }
            ;

            ;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    miviflash.this.startActivity(myIntent);
                }
            }, 5000);




            System.out.println(emp);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    public String getJson()
    {
        String json=null;
        try
        {
            // Opening districtsstates.json file
            InputStream is = getAssets().open("collection.json");
            // is there any content in the file
            int size = is.available();
            byte[] buffer = new byte[size];
            // read values in the byte array
            is.read(buffer);
            // close the stream --- very important
            is.close();
            // convert byte to string
            json = new String(buffer, "UTF-8");
        }
        catch (IOException ex)
        {
            ex.printStackTrace();
            return json;
        }
        return json;
    }

    @Override
    public void onBackPressed() {

    }
}
